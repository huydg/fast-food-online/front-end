var idToDelete = "";
var gOrderArr = [];
const gORDER_COLS = [
    "id",
    "orderDate",
    "user.firstName",
    "user.address",
    "action"
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gORDER_DATE_COL = 1;
const gUserName_COL = 2;
const gADDRESS_COL = 3;
const gACTION_COL = 4;

// Khai báo DataTable & mapping columns
var gUserTable = $("#user-table").DataTable({
    columns: [
        { data: gORDER_COLS[gID_COL] }, 
        { data: gORDER_COLS[gORDER_DATE_COL] }, 
        { data: gORDER_COLS[gUserName_COL] }, 
        { data: gORDER_COLS[gADDRESS_COL] }, 
        { data: gORDER_COLS[gACTION_COL] }, 
    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gACTION_COL,
            defaultContent: `
      <img class="delete-user" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
      <img class="detail-user" src="https://cdn2.iconfinder.com/data/icons/boxicons-solid-vol-1/24/bxs-detail-512.png" style="width: 20px;cursor:pointer;">
    `
    },
    // { // định nghĩa lại cột logo image
    //     targets: gANH_COL,
    //     render: function(data,type,row){
    //         return `<img src="images/${data}", width= 80px />`},
    // }
]
});

function onPageLoading() {
    callApiGetAllOrderDetail();
    loadOrderToTable(gOrderArr);
}

//gán sự kiện detail - xem chi tiết đơn hàng
$("#user-table").on("click", ".detail-user",function(e) {
    onBtnDetailClick(this);
});

//gán sự kiện Delete - Xóa 1 d
$("#user-table").on("click", ".delete-user",function() {
    onBtnDeletelClick(this);
});

$("#btn-confirm-delete-order").on("click", function() {
    onBtnConfirmDelete();
})
function callApiGetAllOrderDetail() {
    $.ajax({
        url: "http://localhost:8080/fastfood/api/noauth/order",
        type: "GET",
        success: function(res) {
           gOrderArr = res;
           console.log(gOrderArr)
           loadOrderToTable(gOrderArr);
        },
        error: function(ajaxContent) {
            alert(ajaxContent);
        }
    })
}

function loadOrderToTable(gOrderArr) {
    gUserTable.clear();
    gUserTable.rows.add(gOrderArr);
    gUserTable.draw();
}

function onBtnDetailClick(button) {
    var data = gUserTable.row($(button).parents('tr')).data();
    console.log(data);
    handleShowInfoModal(data);
    
    $("#exampleModal").modal("show");
}

function onBtnDeletelClick(button) {
    var data = gUserTable.row($(button).parents('tr')).data();
    idToDelete = data.id;
    $("#delete-confirm-modal").modal("show");
}

function onBtnConfirmDelete() {
    console.log(idToDelete);
    $.ajax({
        url: "http://localhost:8080/fastfood/api/noauth/order/" + idToDelete,
        type: "DELETE",
        success: function() {
            toastr.options.preventDuplicates = true;
            toastr.success("Xóa đơn hàng thành công !");
            setTimeout(function(){
                location.reload()}, 3000)
        },
        error:function(err) {
            alert(err);
        }
    })
}
function handleShowInfoModal(data) {
    $.ajax({
        url: `http://localhost:8080/fastfood/api/noauth/order/${data.id}`,
        type: "GET",
        success: function(res) {
            console.log(res);
            $(".modal-body ").append(`<h5 class="modal-title text-uppercase " id="exampleModalLabel">${res.user.lastName} ${res.user.firstName}</h5>
            <p id="show-email" class="mb-4" style="color: #35558a;">${res.user.email}</p>
            <h4 class="mb-5 text-center" style="color: #35558a;">Chi tiết đơn hàng</h4>
            <div class="row text-center">
               <div class="col-4"><b>Tên</b></div> 
               <div class="col-4"><b>Số lượng</b></div> 
               <div class="col-4"><b>Giá</b></div> 
            </div>`)
            for(var i = 0 ; i < res.orderDetails.length; i ++) {
                $(".modal-body ").append(`<div class="row text-center mt-3">
                <div class="col-4">${res.orderDetails[i].product.productName}</div> 
                <div class="col-4">x${res.orderDetails[i].quantity}</div> 
                <div class="col-4 text-muted">${res.orderDetails[i].product.buyPrice}Vnd</div> 
             </div>`);
            }
            $(".modal-body ").append(` <hr class="mt-2 mb-4"
            style="height: 0; background-color: transparent; opacity: .75; border-top: 2px dashed #9e9e9e;">
         <div class="row text-center mt-3">
          <div class="col-4 fw-bold"><b>Tổng hóa đơn:</b></div> 
          <div class="col-4"></div> 
          <div class="col-4 fw-bold" style="color: #35558a;">${res.totalPayment}Vnd</div> 
        </div>`)
          
        },
        error: function(ajaxcontent) {
            alert(ajaxcontent);
        }
    })
}
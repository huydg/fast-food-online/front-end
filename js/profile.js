var userInfomation = {}
$.ajax({
    url: urlInfo,
    method: "GET",
    async: false,
    headers: headers,
    success: function(responseObject) {
        userInfomation = responseObject;
        console.log(userInfomation);
    },
    error: function(xhr) {
        console.log(xhr);
        // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
        //redirectToLogin()
    }
});

showInfo();

function showInfo() {
    $("#show-username").html(userInfomation.username);
    $("#show-email").html(userInfomation.email);
    if(userInfomation.firstName == null) {
        userInfomation.firstName == "";
    } 
    $("#info-fname").val(userInfomation.firstName);

    if(userInfomation.lastName == null) {
        userInfomation.lastName == "";
    } 
    $("#info-lname").val(userInfomation.lastName);

    $("#info-username").val(userInfomation.username);
    $("#info-email").val(userInfomation.email);

    if(userInfomation.phoneNumber == null) {
        userInfomation.phoneNumber == "";
    } 
    $("#info-phone-number").val(userInfomation.phoneNumber);

    if(userInfomation.address == null) {
        userInfomation.address == "";
    } 
    $("#info-address").val(userInfomation.address);

    if(userInfomation.city == null) {
        userInfomation.city == "";
    } 
    $("#info-city").val(userInfomation.city);

    if(userInfomation.state == null) {
        userInfomation.state == "";
    } 
    $("#info-state").val(userInfomation.state);
}

$("#update-info-user").on("click", function() {
    onBtnUpdateClick();
})

function onBtnUpdateClick() {
    var userInfo = {
        firstName: "",
        lastName: "",
        email: "",
        phoneNumber: "",
        address: "",
        city: "",
        state: "",
    }

    getInfoUser(userInfo);
    var isValidInfo = validateFormInfo(userInfo);
    if(isValidInfo) {
        handleUpdateUser(userInfo); 
    }
}

function getInfoUser(userInfo) {
    userInfo.firstName = $("#info-fname").val();
    userInfo.lastName = $("#info-lname").val();
    userInfo.email = $("#info-email").val();
    userInfo.phoneNumber = $("#info-phone-number").val();
    userInfo.address = $("#info-address").val();
    userInfo.city = $("#info-city").val();
    userInfo.state = $("#info-state").val();
}

function validateFormInfo(userInfo) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(userInfo.firstName == "") {
        toastr.options.preventDuplicates = true;
        toastr.error("Bạn cần nhập Tên !");
        return false;
    }
    if(userInfo.lastName == "") {
        toastr.options.preventDuplicates = true;
        toastr.error("Bạn cần nhập Họ !");
        return false;
    }
    if(userInfo.email == "") {
        toastr.options.preventDuplicates = true;
        toastr.error("Bạn cần nhập Email !");
        return false;
    }
    if(!regex.test(userInfo.email)) {
        toastr.options.preventDuplicates = true;
        toastr.error("Email không hợp lệ !");
        return false
    }
    if(userInfo.phoneNumber == "") {
        toastr.options.preventDuplicates = true;
        toastr.error("Bạn cần nhập Số điện thoại !");
        return false;
    }
    if(userInfo.address == "") {
        toastr.options.preventDuplicates = true;
        toastr.error("Bạn cần nhập Địa chỉ !");
        return false;
    }
    if(userInfo.city == "") {
        toastr.options.preventDuplicates = true;
        toastr.error("Bạn cần nhập thành phố !");
        return false;
    }
    if(userInfo.state == "") {
        toastr.options.preventDuplicates = true;
        toastr.error("Bạn cần nhập Quận !");
        return false;
    }
    return true;
} 

function handleUpdateUser(userInfo) {
    $.ajax({
        url: "http://localhost:8080/fastfood/api/noauth/user/" + userInfomation.username,
        type: "PUT",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(userInfo),
        success: function() {
            toastr.options.preventDuplicates = true;
            toastr.success("Cập nhật tài khoản thành công!");
            setTimeout(function(){
                window.location.reload();
             }, 3000);
        },
        error: function(ajaxContent) {
            toastr.options.preventDuplicates = true;
            toastr.error("Cập nhật không thành công!");
        }
        
    })
}


var cartResult = [];
var subTotal = 0;
var nameBankArr = [];
var gh = sessionStorage.getItem("giohang");
var getGioHang = JSON.parse(gh);

    const convert = (getGioHang) => {
      const res = {};
      getGioHang.forEach((obj) => {
         const key = `${obj.id}`;
         if (!res[key]) {
            res[key] = { ...obj, count: 0 };
         };
         res[key].count += 1;
      });
    return Object.values(res);
    };

    cartResult = convert(getGioHang);
    $(".show-order").append(` <div class="row">
    <div class="col-6"><h6><b>Tên</b></h6></div>
    <div class="col-4"><h6><b>Số lượng</b></h6></div>
    <div class="col-2"><h6><b>Giá</b></h6></div>
  </div>`)
    for(var i = 0; i < convert(getGioHang).length; i ++) {
        subTotal += convert(getGioHang)[i].buyPrice*convert(getGioHang)[i].count;
        $(".show-order").append(`
        <div class="row">
              <div class="col-6"><p>${convert(getGioHang)[i].productName}</p></div>
              <div class="col-4"><p>${convert(getGioHang)[i].count}</p></div>
              <div class="col-2"><p>${convert(getGioHang)[i].buyPrice*convert(getGioHang)[i].count}</p></div>
            </div>`)
    }
    $(".show-order").append(`
    <hr>
            <div class="row">
              <div  class="col-6"><p>Tổng tiền:</p></div>
              <div id="total-payment" class="col-6">${subTotal} </div>
            </div>`)

var userInfomation = {}

$.ajax({
    url: urlInfo,
    method: "GET",
    async: false,
    headers: headers,
    success: function(responseObject) {
        userInfomation = responseObject;
        console.log(userInfomation);
    },
    error: function(xhr) {
        console.log(xhr);
        // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
        //redirectToLogin()
    }
}); 

function showInfoPayment() {
  if(userInfomation.firstName == null || userInfomation.lastName == null) {
    userInfomation.firstName == "";
    userInfomation.lastName == "";
  }
  $("#full-name-info").val(userInfomation.lastName + " " + userInfomation.firstName);
  $("#email-info").val(userInfomation.email);
  $("#address-info").val(userInfomation.address);
  $("#city-info").val(userInfomation.city);
  $("#state-info").val(userInfomation.state);
  if(userInfomation.nameBank == null) {
    userInfomation.nameBank == "";
  }
  $("#select-cart").val(userInfomation.nameBank);

  if(userInfomation.nameCard == null) {
    userInfomation.nameCard == "";
  }
  $("#name-card").val(userInfomation.nameCard)

  if(userInfomation.numberCard == null) {
    userInfomation.numberCard == "";
  }
  $("#num-card").val(userInfomation.numberCard);

  if(userInfomation.expiryDate == null) {
    userInfomation.expiryDate == "";
  }
  $("#expmonth-card").val(userInfomation.expiryDate);
}

showInfoPayment();

$("#button-payment").on("click", function() {
  onBtnPaymentClick();
})


function onBtnPaymentClick() {
  var infoPayment = {
    nameBank: "",
    nameCard: "",
    numberCard: "",
    expiryDate: "",
  }

  getInfoPyament(infoPayment);

  console.log(infoPayment);
  var isValidInfoPayment = validateInfoPayment(infoPayment);
  if(isValidInfoPayment) {
    handlePayment(infoPayment);
  }
}

function getInfoPyament(infoPayment) {
  infoPayment.nameBank = $( "#select-cart option:selected" ).text();
  infoPayment.nameCard = $("#name-card").val();
  infoPayment.numberCard = $("#num-card").val();
  infoPayment.expiryDate = $("#expmonth-card").val();
}

function validateInfoPayment(infoPayment) {
  if(infoPayment.nameBank == "" || infoPayment.nameBank == '--Vui lòng chọn ngân hàng--') {
    toastr.options.preventDuplicates = true;
    toastr.error("Bạn cần chọn ngân hàng !");
    return false;
  }

  if(infoPayment.nameCard == "") {
    toastr.options.preventDuplicates = true;
    toastr.error("Bạn cần nhập tên chủ thẻ !");
    return false;
  }

  if(infoPayment.numberCard == "") {
    toastr.options.preventDuplicates = true;
    toastr.error("Bạn cần nhập số thẻ !");
    return false;
  }

  if(infoPayment.expiryDate == "") {
    toastr.options.preventDuplicates = true;
    toastr.error("Bạn cần nhập hạn sử dụng !");
    return false;
  }

  return true;
}

function handlePayment(infoPayment) {
  $.ajax({
    url: "http://localhost:8080/fastfood/api/noauth/user/"+ userInfomation.username +"/payment" ,
    type: "PUT",
    contentType: "application/json;charset=UTF-8",
    data: JSON.stringify(infoPayment),
    success: function() {
        toastr.options.preventDuplicates = true;
        toastr.success("Thanh toán thành công !");
        handlePaymentSuccess();
        setTimeout(function(){
          sessionStorage.removeItem("giohang");
          window.location.href = "index.html";
         }, 3000);
    }, 
    error: function() {
      toastr.options.preventDuplicates = true;
      toastr.success("Thanh toán không thành công !");
    }
})
}

// hàm xử lý khi thanh toán thành công
function handlePaymentSuccess() {
 
  console.log(cartResult)
  var infoPayment = {
    totalPayment: parseInt($("#total-payment").html())
  }
    $.ajax({
      url: "http://localhost:8080/fastfood/api/noauth/order/"+ userInfomation.id ,
      type: "POST",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(infoPayment),
      success: function(res) {
         console.log(res)
          
         // tạo chi tiết đơn hàng
         for(var i = 0 ; i < cartResult.length; i ++) {
          var infoOrderDetail = {
            quantity: cartResult[i].count,
          }
            $.ajax({
              url: `http://localhost:8080/fastfood/api/noauth/orderDetail/${res.id}/${cartResult[i].id}`,
              type: "POST",
              contentType: "application/json;charset=UTF-8",
              data: JSON.stringify(infoOrderDetail),
              success: function() {
                console.log(res);
              }, 
              error: function() {
                alert("lỗi");
              }
            })
         }
         
      }, 
      error: function() {
       
        toastr.success("Gặp lỗi !");
      }
  })
}



